import pandas as pd
import os
import glob
from tqdm import tqdm,trange

def get_patient_dict(f,clin=False, masaoka=False):
    d = {}
    if clin == True:
        df = pd.read_table(f, sep='\t', index_col=0)
        patients_list = df.loc['patient.bcr_patient_barcode'].tolist()
        stages_list = df.loc['patient.stage_event.clinical_stage'].tolist()
    
    elif masaoka == True:
        df = pd.read_table(f, sep='\t', index_col=0)
        patients_list = df.loc['patient.bcr_patient_barcode'].tolist()
        stages_list = df.loc['patient.stage_event.masaoka_stage'].tolist()        
    else:
        df = pd.read_table(f, sep='\t', index_col=0)
        patients_list = df.loc['patient.bcr_patient_barcode'].tolist()
        stages_list = df.loc['patient.stage_event.pathologic_stage'].tolist()

    for p in range(len(patients_list)):
        if not pd.isnull(stages_list[p]):
            d[patients_list[p].upper()] = stages_list[p].replace(' ', '_')
        else:
            d[patients_list[p].upper()] = str(stages_list[p])

    return d


if __name__ == '__main__':

    patient_subtype_file = '../data/pancancer_21_patient_subtypes.txt'
    mutation_file = '../data/pancancer_21_mutations.txt'
    cancer_types_file = '../data/cancers.txt'
    stage_info_file = '../data/stage_info.txt'

    clinical_list = ['CESC', 'OV', 'UCEC', 'UCS']
    
    with open(cancer_types_file, 'r') as f:
        cancer_types = [c.split()[0] for c in f.readlines()]

    mf = open(mutation_file, 'w')
    sf = open(patient_subtype_file, 'w')
    stf = open(stage_info_file, 'w')
    
    for c in tqdm(cancer_types):#['ACC','BRCA', 'CESC', 'ESCA', 'HNSC', 'LIHC', 'READ']:
        print c
        type_mutation_file = '../data/' + c + '_patient_vs_genes.txt'
        inpath = '../data/tcga/' + c + '_oncotated/'
        clin_data = '../data/clin_data/' + c + '.clin.merged.txt'

        patient_stages_dict = {}
        stage_patient_dict = {}
        try:
            if c in clinical_list:
                patient_stages_dict = get_patient_dict(clin_data, clin=True)
            elif c == 'THYM':
                patient_stages_dict = get_patient_dict(clin_data, masaoka=True)
            else:
                patient_stages_dict = get_patient_dict(clin_data)
        except:
            print 'ERROR: ' + c + 'NOT FOUND!!'
            continue
        #stage_list = list(set([s for s in patient_stages_dict.values()]))
        #print stage_list
        for p in patient_stages_dict:
            s = patient_stages_dict[p]
            if s in stage_patient_dict:
                stage_patient_dict[s].update([p])
            else:
                stage_patient_dict[s] = set([p])
        
        stf.write(c + '\n')
        for s in stage_patient_dict:
            stf.write(str(s) + '\t' + str(len(stage_patient_dict[s])) + '\n')
        stf.write('\n')
        
        file_lists = glob.glob(inpath+'TCGA*.txt')

        with open(type_mutation_file, 'w') as tf:
            for file_ in tqdm(file_lists):
                directory, file_core = os.path.split(file_)
                patient = file_core[:12]
                if patient not in patient_stages_dict:
                    print 'ERROR: Patient ' + patient + ' not found!!'
                    continue
                        
                with open(file_, 'r') as f:
                    mutations, variant = [],[]
                    lines = f.readlines()[4:]
                    for line in lines:
                        line = line.split()
                        mutations.append(line[0])
                        variant.append(line[8])
                mf.write(patient)
                tf.write(patient)
                for m in range(len(mutations)):
                    if variant[m] != 'Silent':
                        mf.write('\t' + mutations[m])
                        tf.write('\t' + mutations[m])
                mf.write('\n')
                tf.write('\n')
                if c == 'THYM':
                    sf.write(patient + '\t' + c + '\tstage_' + str(patient_stages_dict[patient])+'\n')
                else:
                    sf.write(patient + '\t' + c + '\t' + str(patient_stages_dict[patient])+'\n')


