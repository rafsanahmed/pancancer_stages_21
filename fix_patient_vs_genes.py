
inpath = '../data/'
outpath = '../data/tcga_patient_vs_genes/'
cancer_file = inpath + 'cancers.txt'

with open(cancer_file, 'r') as f:
    cancer_types = [line.split()[0] for line in f.readlines()]

for c in cancer_types:

    infile = inpath + c+ '_patient_vs_genes.txt'
    outfile = outpath + c+ '_patient_vs_genes.txt'

    d = {}
    with open(infile, 'r') as f:

        lines = f.readlines()

        if len(lines) == 0:
            continue

        for line in lines:
            line = line.split()
            d[line[0]] = set(line[1:])

    with open(outfile,'w') as f:

        for key, sets in sorted(d.iteritems()):

            f.write(key)

            for g in sorted(sets):
                gg = g.upper()
                if gg != 'UNKNOWN':
                    f.write('\t' + gg)
            f.write('\n')

