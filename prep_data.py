import pandas as pd

patient_subtype_file = '../data/our_patients_subtypes.txt'
outfile = '../data/patient_cancer_types_stages.txt'
stage_info_outfile = '../data/pancancer_stage_info.txt'
cancer_types_pat = ['BLCA', 'BRCA', 'HNSC', 'KIRC', 'LUAD', 'LUSC']
cancer_types_clin = ['OV', 'UCEC' ]
cancer_types_crc = ['COAD', 'READ']

with open(patient_subtype_file, 'r') as f:
    patients_mut = set([p.split()[0] for p in f.readlines()])

f= open(outfile, 'w')
sf = open(stage_info_outfile, 'w')

for c in cancer_types_pat:
    print 'type: ' + c
    #fields = ['patient.bcr_patient_barcode', 'patient.stage_event.pathologic_stage']
    df = pd.read_table('../merged_files/'+c+'.clin.merged.txt', sep='\t',index_col=0)
    patients = df.loc['patient.bcr_patient_barcode'].tolist()
    stages = df.loc['patient.stage_event.pathologic_stage'].tolist()

    #getting patient's stage informations
    stages_dict = {}
    for s in set(stages):
        stages_dict[s] = set()

    for i in range(len(stages)):
        if patients[i].upper() in patients_mut:
            stages_dict[stages[i]].update([patients[i]])

    for k in stages_dict.keys():
        sf.write(c + '\t' + str(k) + '\t' + str(len(stages_dict[k])) + '\n')
    sf.write('\n')

    #getting patient info
    for i in range(len(patients)):
        p = patients[i].upper()
        if p in patients_mut and not pd.isnull(stages[i]):
            f.write(p + '\t' + c + '\t' + stages[i] + '\n')

for c in cancer_types_clin:
    print 'type: ' + c
    #fields = ['patient.bcr_patient_barcode', 'patient.stage_event.clinical_stage']
    df = pd.read_table('../merged_files/' + c + '.clin.merged.txt', sep='\t', index_col=0)
    patients = df.loc['patient.bcr_patient_barcode'].tolist()
    stages = df.loc['patient.stage_event.clinical_stage'].tolist()

    # getting patient's stage informations
    stages_dict = {}
    for s in set(stages):
        stages_dict[s] = set()

    for i in range(len(stages)):
        if patients[i].upper() in patients_mut:
            stages_dict[stages[i]].update([patients[i]])

    for k in stages_dict.keys():
        sf.write(c + '\t' + str(k) + '\t' + str(len(stages_dict[k])) + '\n')
    sf.write('\n')

    #getitng patient info
    for i in range(len(patients)):
        p = patients[i].upper()
        if p in patients_mut and not pd.isnull(stages[i]):
            f.write(p + '\t' + c + '\t' + stages[i] + '\n')

main_stages_dict = {}
for c in cancer_types_crc:
    print 'type: ' + c
    #fields = ['patient.bcr_patient_barcode', 'patient.stage_event.clinical_stage']
    df = pd.read_table('../merged_files/' + c + '.clin.merged.txt', sep='\t', index_col=0)
    patients = df.loc['patient.bcr_patient_barcode'].tolist()
    stages = df.loc['patient.stage_event.pathologic_stage'].tolist()

    # getting patient's stage informations
    stages_dict = {}
    for s in set(stages):
        stages_dict[s] = set()

    for i in range(len(stages)):
        if patients[i].upper() in patients_mut:
            stages_dict[stages[i]].update([patients[i]])

    for k in stages_dict.keys():
        sf.write(c + '\t' + str(k) + '\t' + str(len(stages_dict[k])) + '\n')
    sf.write('\n')

    #getitng patient info
    for i in range(len(patients)):
        p = patients[i].upper()
        if p in patients_mut and not pd.isnull(stages[i]):
            f.write(p + '\t' + 'CRC' + '\t' + stages[i] + '\n')



