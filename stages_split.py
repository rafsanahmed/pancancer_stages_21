import pandas as pd
from collections import OrderedDict
from tqdm import tqdm

patient_subtype_file = '../data/our_patients_subtypes.txt'
patient_indices_file = '../data/patients_to_indices_gen.txt'
infile = '../data/patient_cancer_types_stages.txt'
patient_vs_genes_file = '../data/patient_data_gen_ready.txt'

stage_i_subs = ['stage_i', 'stage_ia', 'stage_ib', 'stage_ic']
stage_ii_subs = ['stage_ii', 'stage_iia', 'stage_iib', 'stage_iic']
stage_iii_subs = ['stage_iii', 'stage_iiia', 'stage_iiib', 'stage_iiic', 'stage_iiic1', 'stage_iiic2']
stage_main = {'stage_i_comb':set(),'stage_ii_comb':set(), 'stage_iii_comb':set()}


with open(patient_indices_file, 'r') as f:
    patient_indices = {line.split()[0]:line.split()[1] for line in f.readlines()}

with open(patient_vs_genes_file, 'r') as f:
    patient_vs_genes = {line.split()[0]:line.split()[1:] for line in f.readlines()}

stage_dict = {k:set() for k in stage_i_subs+stage_ii_subs+stage_iii_subs}

with open(infile, 'r') as f:
    lines = f.readlines()

    for line in lines:
        line = line.split()
        # if line[2] in stage_dict:
        #     stage_dict[line[2]].update([line[0]])

        if line[2] in stage_i_subs:
            stage_main['stage_i_comb'].update([line[0]])
        elif line[2] in stage_ii_subs:
            stage_main['stage_ii_comb'].update([line[0]])
        elif line[2] in stage_iii_subs:
            stage_main['stage_iii_comb'].update([line[0]])

# for k in stage_dict.keys():
#     print k, len(stage_dict[k])
# print
for k in tqdm(stage_main.keys()):
    #print k, len(stage_main[k])

    outfile = '../data/stages/' + str(k) + '_patient_indices.txt'
    gene_set = set()
    gene_pat_dict = {}
    total_patients = float(len(stage_main[k]))

    with open(outfile, 'w') as f:

        for p in sorted(stage_main[k]):
            f.write(p + '\t' + patient_indices[p] + '\n')
            gene_set.update(patient_vs_genes[p])

    outfile2 = '../data/stages/'+ str(k) + '_gene_vs_patients.txt'

    with open(outfile2, 'w') as f:
        for gene in gene_set:
            if gene not in gene_pat_dict:
                gene_pat_dict[gene] = set()
            for p in sorted(stage_main[k]):
                if gene in patient_vs_genes[p]:
                    gene_pat_dict[gene].update([p])
        for gene in gene_pat_dict.keys():
            f.write(gene)
            for p in gene_pat_dict[gene]:
                f.write('\t' + patient_indices[p])
            f.write('\n')


    outfile3 = '../data/stages/'+ str(k) + '_mut_freq.txt'

    with open(outfile3, 'w') as f:

        for gene in tqdm(gene_pat_dict.keys()):
            f.write(gene + '\t' + str(float(len(gene_pat_dict[gene]))/total_patients) + '\n')






